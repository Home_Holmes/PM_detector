#include <Arduino.h>
#include <Wire.h>
#define __EEPROM_ADDR       0x50
#define __Pointer_ADDR       0
#define __Init_ADDR         1000
typedef struct _profile_storage
{ 
    unsigned int Cur_Storage_ADDR; 
    unsigned int Cur_Storage_SIZE; 
    unsigned int Cur_pointer_ADDR;
    
} profile_t;
void Init_eeprom(profile_t profile);
void i2c_eeprom_read_buffer( int deviceaddress, unsigned int eeaddress, uint8_t *buffer, int length );
void i2c_eeprom_write_page( int deviceaddress, unsigned int eeaddresspage, uint8_t* data, uint8_t length );
void i2c_eeprom_write_byte( int deviceaddress, unsigned int eeaddress, uint8_t data );
byte i2c_eeprom_read_byte( int deviceaddress, unsigned int eeaddress );
unsigned int i2c_eeprom_write_byte_array( int deviceaddress, unsigned int eeaddress, uint8_t *data );


/*


|Cur_Storage_ADDR_ADDR|Cur_Storage_ADDR
    Storage

*/
