#include "EEPROM.h"
void Init_eeprom(profile_t profile)
{
  uint8_t buf[sizeof(profile.Cur_Storage_ADDR)];
  //i2c_eeprom_write_byte(__EEPROM_ADDR,__Pointer_ADDR,__Init_ADDR)
  i2c_eeprom_read_buffer(__EEPROM_ADDR,__Pointer_ADDR,buf,sizeof(profile.Cur_Storage_ADDR));
  delay(10);
  memcpy(&profile.Cur_Storage_ADDR,buf,sizeof(profile.Cur_Storage_ADDR));
  Serial.print("Cur_Storage_ADDR:");
  Serial.println(profile.Cur_Storage_ADDR);
  profile.Cur_pointer_ADDR=profile.Cur_Storage_ADDR;
}
void i2c_eeprom_read_buffer( int deviceaddress, unsigned int eeaddress, uint8_t *buffer, int length )
{
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));    // Address High Byte
  Wire.write((int)(eeaddress & 0xFF));  // Address Low Byte
  Wire.endTransmission();
  Wire.requestFrom(deviceaddress,length);
  for ( int c = 0; c < length; c++ )
    if (Wire.available()) buffer[c] = Wire.read();
}

byte i2c_eeprom_read_byte( int deviceaddress, unsigned int eeaddress )
{
  byte rdata = 0xFF;
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));    // Address High Byt
  Wire.write((int)(eeaddress & 0xFF));  // Address Low Byte
  Wire.endTransmission();
  Wire.requestFrom(deviceaddress,1);
  if (Wire.available()) rdata = Wire.read();
  return rdata;
}

void i2c_eeprom_write_byte( int deviceaddress, unsigned int eeaddress, uint8_t data )
{
  int rdata = data;
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));    // Address High Byte
  Wire.write((int)(eeaddress & 0xFF));  // Address Low Byte
  Wire.write(rdata);
  Wire.endTransmission();
}
unsigned int i2c_eeprom_write_byte_array( int deviceaddress, unsigned int eeaddress, uint8_t *data )
{ 
  
  for(int i =0;i<sizeof(data);i++)
    {
      i2c_eeprom_write_byte(__EEPROM_ADDR,eeaddress+i,data[i]);
      delay(10);
    }
  eeaddress=eeaddress+sizeof(data);
  return eeaddress;
}

// Address is a page address, 6-bit (63). More and end will wrap around
// But data can be maximum of 28 bytes, because the Wire library has a buffer of 32 bytes
void i2c_eeprom_write_page( int deviceaddress, unsigned int eeaddresspage, uint8_t* data, uint8_t length )
{
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddresspage >> 8)); // Address High Byte
  Wire.write((int)(eeaddresspage & 0xFF)); // Address Low Byte
  uint8_t c;
  for ( c = 0; c < length; c++)
    Wire.write(data[c]);
  Wire.endTransmission();
  delay(10);                           // need some delay
}