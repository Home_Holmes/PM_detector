#include <Arduino.h>
#include <Wire.h>
#include "DHT.h"
#include <DS3232RTC.h>      // https://github.com/JChristensen/DS3232RTC
#include <Streaming.h>      // http://arduiniana.org/libraries/streaming/
#include <time.h>           // http://playground.arduino.cc/Code/Time
#include <EEPROM.h>
#define DHTPIN 1     // what pin we're connected to
#define DHTTYPE DHT11 // DHT 11

#define __Set_timer_St          0
#define __Waiting_St            1
#define __Humid_St              2
#define __PARICALE_St           3
#define __Time_St               4 

//#define DEBUG


typedef struct _sensor_val
{  
    float mass_conc_10;
    float mass_conc_25;
    float mass_conc_40;
    float mass_conc_100;
    float number_conc_05;
    float number_conc_10;
    float number_conc_25;
    float number_conc_40;
    float number_conc_100;
    float typical_particle_size;
    float humid;
    float temp;
    //float latitude, longitude;
    int hour;
    int mins;
    //int sec;
    int day;
    int month;
    int year; 
    uint8_t Rawdata[60];
    
} sensor_val_t; 

typedef struct _state
{
  
    bool WRITE_VALUE_flag;
    bool SEND_VALUE_flag;
    int Terminal_state;
    int READ_VALUE_state;
    int WRITE_VALUE_state;
    bool flagExecRequest = false;
    bool flagTerminalResume = true;

} state_t;

#pragma region TASK
	#define STATIC_TIMER_UPDATE timer = millis() + duration					       // Reschedule for next calling
  #define STATIC_TIMER_INIT static unsigned long timer = millis() + duration			// Schedule Variable Declaration
  #define STATIC_TIMER_CHECK (millis() >= timer)							         // Schedule Checking

  bool task_run();
  bool task_stop();
  bool task_isRunning();

  bool isControlTaskEnabled = false;
  void task_READ_VALUE(long duration);
  void task_WRITE_VALUE(long duration);
  void task_SEND_VALUE(long duration);
  void task_Terminal(long duration);
  void task_Exec();
#pragma endregion

#pragma region READ VALUE
  #define __READ_Duration 10
  #define __READ_TIMER    1000*60*5//1*60*60*1000
    #pragma region HUMID
      void Init_dht11();
      void getdata_dht11(sensor_val_t *sensor_value_p);
    #pragma endregion

    #pragma region PARTICLE
      #define __SPS30_ADDR 0x69
      #define __INTERVAL_CLEANING 0x00000708 //(30min)
      uint8_t CalcCrc(uint8_t data[2]);
      void Init_sps30();
      void getdata_sps30(sensor_val_t *sensor_value_p);   
      uint8_t buffer[] = {0x03,0x00};
      uint8_t check_state[] ={0x02,0x02};
      uint8_t read[] = {0x03,0x00};
    #pragma endregion

  #pragma region TIME
    void Init_rtc(void);
    void getdata_RTC(sensor_val_t *sensor_value_p);
  #pragma endregion
#pragma endregion

#pragma region WRITE VALUE
    #define __WRITE_TIMER        1*60*60*1000
    #define __WRITE_Duration     10

#pragma endregion


#pragma region SEND VALUE
  #define __SEND_TIMER    1*60*60*1000
  #define __SEND_Duration 10
  
#pragma endregion

#pragma region TERMINAL
  #define __Terminal_Duration         10
  #define COMMAND_MAX_LENGTH          50
  char bufferString[COMMAND_MAX_LENGTH];
  int bufferLength = 0;
  char bufferChar;
  char command[COMMAND_MAX_LENGTH];
  int commandLength = 0;
#pragma endregion
  
#pragma region GLOBAL
    sensor_val_t    sensor_value;
    sensor_val_t    sensor_value_temp;
    state_t         state;
    profile_t       profile;
    DHT dht(DHTPIN, DHTTYPE);
    DS3232RTC myRTC(false); 
    float getfloat(uint8_t data[],int i);
    bool isMatch(char *p, char *keyword);
#pragma endregion

void setup() 
{
    delay(5000);
    task_run();
}
void loop() 
{
  if (task_isRunning())
	  {
		  task_READ_VALUE(__READ_Duration);
      task_WRITE_VALUE(__WRITE_Duration);
      task_SEND_VALUE(__SEND_Duration);	
	  }
  task_Terminal(__Terminal_Duration);
  task_Exec();
    
}

bool task_run()
{ 
  Wire.begin();
  Serial.begin(9600); // put your setup code here, to run once:
  Init_dht11();
  Init_sps30();
  Init_eeprom(profile);
  Init_rtc();
  isControlTaskEnabled = true;
  state.READ_VALUE_state  = __Set_timer_St;
  state.WRITE_VALUE_state = __Set_timer_St;
  return true;
}
bool task_stop()
{ 
  isControlTaskEnabled = false; 
  return true;
}
bool task_isRunning()
{
  return isControlTaskEnabled;
}

void task_READ_VALUE(long duration)
{
  STATIC_TIMER_INIT;
  static unsigned long  read_timer;
	if (STATIC_TIMER_CHECK) 
    {
        switch(state.READ_VALUE_state)
        {
        case __Set_timer_St:
            read_timer= millis() + __READ_TIMER ;
            state.READ_VALUE_state=__Time_St;
        break;
        case __Waiting_St:
            if(millis() >= read_timer)
            {   
                read_timer= read_timer + __READ_TIMER;
                state.READ_VALUE_state=__Time_St;
            }
            
        break;
        case __Time_St:
            getdata_RTC(&sensor_value);
            state.READ_VALUE_state=__Humid_St;
            
        break;

        case __Humid_St:
            getdata_dht11(&sensor_value);
            state.READ_VALUE_state=__PARICALE_St;
        break;

        case __PARICALE_St:
            getdata_sps30(&sensor_value);
            state.READ_VALUE_state=__Waiting_St;
            state.WRITE_VALUE_flag=true;
        break;

     
            
        

        }
  STATIC_TIMER_UPDATE;  
	}
  
}

void task_WRITE_VALUE(long duration)
{  
  STATIC_TIMER_INIT;
  static unsigned long  write_timer;
	if (STATIC_TIMER_CHECK) 
  {
     switch(state.WRITE_VALUE_state)
      { 
        case __Set_timer_St:
          write_timer= millis() + __WRITE_TIMER ;
          state.WRITE_VALUE_state=__Time_St;
        break;
        case __Waiting_St:
          if((millis() >= write_timer))
          {   
            write_timer= write_timer + __READ_TIMER;
            sensor_value_temp=sensor_value;
            state.WRITE_VALUE_state=__Time_St;
            
          }
        break;

        case __Time_St:
          
          if(state.WRITE_VALUE_flag)
          {
          state.WRITE_VALUE_flag= false;
          uint8_t buf11[sizeof(sensor_value_temp.day)];
          memcpy(&buf11[0],&sensor_value.hour,sizeof(sensor_value_temp.day));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf11);

          uint8_t buf12[sizeof(sensor_value.month)];
          memcpy(&buf12[0],&sensor_value.month,sizeof(sensor_value.month));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf12);

          uint8_t buf1[sizeof(int(sensor_value.hour))];
          memcpy(&buf1[0],&sensor_value.hour,sizeof(sensor_value.hour));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf1);
          
          uint8_t buf2[sizeof(int(sensor_value.mins))];
          memcpy(&buf2[0],&sensor_value.mins,sizeof(sensor_value.mins));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf2);
          
          
          state.WRITE_VALUE_state=__Humid_St;
          
          }
        break;

        case __Humid_St:
          
          uint8_t buf3[sizeof(sensor_value.humid)];
          memcpy(&buf3[0],&sensor_value.humid,sizeof(sensor_value.humid));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf3);
         
          uint8_t buf4[sizeof(sensor_value.temp)];
          memcpy(&buf4[0],&sensor_value.temp,sizeof(sensor_value.temp));
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,buf4);
          
          state.WRITE_VALUE_state=__PARICALE_St;
         
        break;

        case __PARICALE_St:
          profile.Cur_pointer_ADDR=i2c_eeprom_write_byte_array(__EEPROM_ADDR,profile.Cur_pointer_ADDR,sensor_value.Rawdata);                 
          if(profile.Cur_Storage_ADDR>=(25600*((profile.Cur_Storage_ADDR-__Init_ADDR)/sizeof(profile.Cur_Storage_ADDR))+1))
          {
            profile.Cur_Storage_ADDR=__Init_ADDR;
          }
          i2c_eeprom_write_byte(__EEPROM_ADDR,__Pointer_ADDR,profile.Cur_Storage_ADDR);
          Serial.print("save complete");
          state.WRITE_VALUE_state=__Waiting_St;
          

        break;
      
        }
  STATIC_TIMER_UPDATE;
  }
  

		
}


void task_SEND_VALUE(long duration)
{
  STATIC_TIMER_INIT;
	if (STATIC_TIMER_CHECK) 
    {
    
      if(state.SEND_VALUE_flag)
      {
        state.SEND_VALUE_flag=false;
      }

    STATIC_TIMER_UPDATE;
    }

}

void task_Terminal(long duration)
{
	STATIC_TIMER_INIT;
	if (STATIC_TIMER_CHECK) {
		if (state.flagTerminalResume) {
			Serial.print(F(">> "));
			state.flagTerminalResume = false;
		}					
		while (Serial.available() > 0) // TODO: Watchdog Timer Required.
		{
			if (!state.flagExecRequest) {
				bufferChar = (char)Serial.read();
				if (bufferChar == '\n' || bufferChar== '\r') {
					if (bufferLength > 0) {
						bufferString[bufferLength] = '\0';
						memcpy(command, bufferString, bufferLength + 1);
						commandLength = bufferLength;
						bufferLength = 0;
						state.flagExecRequest = true;
						Serial.println();
						break;
					} 
				}
				else if (bufferChar == '\b' || bufferChar == 127) {  // backspace for windows, putty
					if (bufferLength >= 1) {
						bufferLength--;
						Serial.print(bufferChar);
					}
					else {
						bufferLength = 0;
					}
				}
				else if (bufferChar == ' ') {
					if ((bufferLength > 0) && (bufferString[bufferLength-1] != ' ')) {
						Serial.print(bufferChar);
						bufferString[bufferLength++] = bufferChar;
					}
				}
				else {
					Serial.print(bufferChar);
					bufferString[bufferLength++] = bufferChar;
				}
			}
		}
		STATIC_TIMER_UPDATE;
	}
}
void task_Exec()
{
    if (state.flagExecRequest)
     {
		

		if (isMatch(command, "showall")) {
      
     
      for(int i=__Pointer_ADDR;i<__Pointer_ADDR+8+76*profile.Cur_Storage_SIZE;i++)
      {
        Serial.print(i2c_eeprom_read_byte(__EEPROM_ADDR,i));
        Serial.print("|");
        if(i==7) {Serial.println("");}
        
        else if((i-7)%76==0)
        {
        Serial.println("");
        Serial.println("");
        }
      }
			 
		}
		else if (isMatch(command,"clear"))
    {
      profile.Cur_Storage_ADDR=__Pointer_ADDR+sizeof(profile.Cur_Storage_ADDR)+sizeof(profile.Cur_Storage_SIZE);
      profile.Cur_Storage_SIZE =0;
      
      uint8_t buf1[sizeof(profile.Cur_Storage_ADDR)];
      memcpy(buf1,&profile.Cur_Storage_ADDR,sizeof(profile.Cur_Storage_ADDR));
      i2c_eeprom_write_byte_array(__EEPROM_ADDR,__Pointer_ADDR,buf1);

      memcpy(buf1,&profile.Cur_Storage_SIZE,sizeof(profile.Cur_Storage_ADDR)) ;
      i2c_eeprom_write_byte_array(__EEPROM_ADDR,__Pointer_ADDR+sizeof(profile.Cur_Storage_ADDR),buf1);//
		}
    else if (isMatch(command, "help")) {
      Serial.println("cmd list");
      Serial.println("-show ");
      Serial.println("-showall");
      Serial.println("-clear ");

			 
		}
    else if(isMatch(command, "show"))
    { Serial<<"Time  :"         << sensor_value.hour<<"."<<sensor_value.mins<<endl;
      Serial<<"Humid :"   << sensor_value.humid<<"  Temp:"<<sensor_value.temp<<endl;
      Serial<<"mass_conc_10   :"         << getfloat(sensor_value.Rawdata,0)<<endl;
      Serial<<"mass_conc_25   :"         << getfloat(sensor_value.Rawdata,6)<<endl;
      Serial<<"mass_conc_40   :"         << getfloat(sensor_value.Rawdata,12)<<endl;
      Serial<<"mass_conc_100  :"        << getfloat(sensor_value.Rawdata,18)<<endl;
      Serial<<"number_conc_05 :"        << getfloat(sensor_value.Rawdata,24)<<endl;
      Serial<<"number_conc_10 :"       << getfloat(sensor_value.Rawdata,30)<<endl;
      Serial<<"number_conc_25 :"       << getfloat(sensor_value.Rawdata,36)<<endl;
      Serial<<"number_conc_40 :"       << getfloat(sensor_value.Rawdata,42)<<endl;
      Serial<<"number_conc_100 :"      << getfloat(sensor_value.Rawdata,48)<<endl;
      Serial<<"typical_particle_size:" << getfloat(sensor_value.Rawdata,54)<<endl;
    }
		else {
			Serial.println("Unknown Command!. Try 'help'.");
		}
		Serial.println();
		state.flagExecRequest = false;
		state.flagTerminalResume = true;
	}
}



/*-------------------------------------------------------------------------------------------------------------------------------------*/
bool isMatch(char *p, char *keyword)
{
	int i, len = strlen(keyword);
	for (i = 0; i < len; i++)
	{
		if (tolower(p[i]) != tolower(keyword[i])) {
			break;
		}
	}
	return (i == len);
}

void Init_rtc(void)
{
  myRTC.begin();
  //myRTC.set(now());                     
}
void getdata_RTC(sensor_val_t *sensor_value_p)
{
  setSyncProvider(myRTC.get); 

  sensor_value_p->hour=hour();
  sensor_value_p->mins=minute();
  
  sensor_value_p->day=day();
  sensor_value_p->month=month();
  sensor_value_p->year =year();
}

uint8_t CalcCrc(uint8_t data[2]) 
{
  uint8_t crc = 0xFF;
  for(int i = 0; i < 2; i++) 
  {
    crc ^= data[i];
    for(uint8_t bit = 8; bit > 0; --bit) 
    {
      if(crc & 0x80) 
      {
      crc = (crc << 1) ^ 0x31u;
      } 
      else 
      {
        crc = (crc << 1);
      }
    }
  }
  return crc;
} 

void Init_sps30()
{
  uint8_t start[5];
  start[0] = 0x00;
  start[1] = 0x10;
  start[2] = 0x03;
  start[3] = 0x00;
  start[4] = CalcCrc(buffer);
  Wire.beginTransmission((__SPS30_ADDR)); // transmit to sps30 
  for(int i=0;i<5;i++)
  {
    Wire.write(start[i]);              // sends cmd start measurement
  }     
  Wire.endTransmission(); // stop transmitting

  
  uint8_t set_clean[8];
  set_clean[0] = 0x80;
  set_clean[1] = 0x01;
  set_clean[2] = 0xFF&(__INTERVAL_CLEANING>>24);
  set_clean[3] = 0xFF&(__INTERVAL_CLEANING>>16);

  uint8_t cal_buf[2];
  cal_buf[0]=set_clean[2];
  cal_buf[1]=set_clean[3];
  set_clean[4]= CalcCrc(cal_buf);

  set_clean[5]= 0xFF&(__INTERVAL_CLEANING>>8);
  set_clean[6]= 0xFF&(__INTERVAL_CLEANING);
  cal_buf[0]=set_clean[5];
  cal_buf[1]=set_clean[6];
  set_clean[7]= CalcCrc(cal_buf);
  Wire.beginTransmission((__SPS30_ADDR)); // transmit to device #8  
  for(int i=0;i<8;i++)
  {
    Wire.write(set_clean[i]);
  }             
  Wire.endTransmission(); // stop transmitting
  Serial.println("Inint_AutoFan");
  
}

void getdata_sps30(sensor_val_t *sensor_value_p)
{
  uint8_t startread;
  uint8_t pmState;
  while(1)
  
  { 
    
    Wire.beginTransmission(((__SPS30_ADDR)));
    for(int i=0;i<2;i++)
    {
      Wire.write(check_state[i]);              // sends one byte
    }               
    Wire.endTransmission(); 
    Wire.requestFrom(((__SPS30_ADDR)),2);
    startread=Wire.read();  
    pmState= Wire.read();
    if(pmState==0x01)
      { 
        Serial.print("r2read");
        Wire.beginTransmission(((__SPS30_ADDR)));
        for(int i=0;i<2;i++)
        {
          Wire.write(read[i]);
        }
        Wire.endTransmission();
        Wire.requestFrom(((__SPS30_ADDR)),60);
      
        if(Wire.available())
        {
        for(int i=0;i<60;i++)
          {
            sensor_value_p->Rawdata[i]=Wire.read();
            
            
          }
        }
      sensor_value_p->mass_conc_10    =getfloat(sensor_value_p->Rawdata,0);
      sensor_value_p->mass_conc_25    =getfloat(sensor_value_p->Rawdata,6);
      sensor_value_p->mass_conc_40    =getfloat(sensor_value_p->Rawdata,12);
      sensor_value_p->mass_conc_100   =getfloat(sensor_value_p->Rawdata,18);
      sensor_value_p->number_conc_05  =getfloat(sensor_value_p->Rawdata,24);
      sensor_value_p->number_conc_10  =getfloat(sensor_value_p->Rawdata,30);
      sensor_value_p->number_conc_25  =getfloat(sensor_value_p->Rawdata,36);
      sensor_value_p->number_conc_40  =getfloat(sensor_value_p->Rawdata,42);
      sensor_value_p->mass_conc_100   =getfloat(sensor_value_p->Rawdata,48);
      sensor_value_p->typical_particle_size= getfloat(sensor_value_p->Rawdata,54);
      Serial.print("finish");
     break;
    }
  }
  
  
}
 
void Init_dht11()
{
  dht.begin();
}
void getdata_dht11(sensor_val_t *sensor_value_p)
{
  float t =dht.readTemperature();
  float h =dht.readHumidity();
  if (isnan(t) || isnan(h)) 
  {
    Serial.println("Failed to read from DHT");
  } 
  else 
  {
    sensor_value_p->humid =h;
    sensor_value_p->temp = t;
  }
}




float getfloat(uint8_t data[],int i)
{
  uint8_t	 vBuffer[4];
  uint8_t	 calBuffer1[2];
  uint8_t	 calBuffer2[2];
  calBuffer1[0]=data[i];
  calBuffer1[1]=data[i+1];
  calBuffer2[0]=data[i+3];
  calBuffer2[1]=data[i+4];
   if(data[i+2]==CalcCrc(calBuffer1) && data[i+5]==CalcCrc(calBuffer2))
  {
    vBuffer[0]=data[i+4];
    vBuffer[1]=data[i+3];
    vBuffer[2]=data[i+1];
    vBuffer[3]=data[i];
    float result ;
    memcpy(&result, &vBuffer[0], sizeof(float));
    return result;
  }
  else
    {
      return -1;
    }
  
}

